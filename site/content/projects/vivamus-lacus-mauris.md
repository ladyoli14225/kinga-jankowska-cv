+++
date = " "
title = "My projects & Hackathons"
type = "service"
+++

## Winner of Harmony.ONE Hackathon

![geospatia](../images/geospatia.PNG)

Map-based NFT Marketplace. Technologies: web3/metamask, Harmony.One. 

Gitcoin bounty link: https://gitcoin.co/issue/harmony-one/bounties/48/100025830, 

Youtube demo: https://www.youtube.com/watch?v=WV7UHb4XQ6Q

## Protocol - E2E Encrypted and anonymous chat app

![protocol](../images/protocol1.PNG)

https://thecyberd3m0n.gitlab.io/protocol-im/home

Easy to use, secure, private. Encrypts transport as well as local database. Allows only text (for now). Inviting new people don’t requires to install the app by them.
