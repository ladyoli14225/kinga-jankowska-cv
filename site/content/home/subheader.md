+++
type = "subheader"
title = ""
+++

I am an **Angular developer** and an experienced **Tester**, proficient in testing web applications and crafting front-end solutions. My unique combination of expertise enables me to effectively identify and resolve issues, while also delivering high-quality and user-friendly interfaces. With a passion for both development and testing, I bring a holistic approach to creating robust and seamless digital experiences.

