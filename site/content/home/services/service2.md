+++
title = "TCD Software"
dateRange = "2021 - 2023"
type = "service"
+++

---

Various projects, where I gained valuable experience in developing web applications using Angular framework, collaborating with diverse clients.

<!--more-->

## Projects:

#### *Multihodl*

![multihodl](../../../images/multihodl.PNG)

https://multihodl.network/

Web 3.0 project for sharing, creating and splitting the crypto funds. It allows to create single ETH address that splits funds between shareholders.
Project uses Truffle for Smart Contracts management and Angular Framework for frontend.

---

#### *Mentoria*

![mentoria](../../../images/mentoria.PNG)

https://mentoria-dev.ipv8.pl/app

The online video platform designed specifically for mentors, offering them a seamless experience to upload and stream their video content.  Additionally, the platform allows viewers to engage with the content by leaving comments.
