+++
title = "JMM Development"
dateRange = "2020 - 2021"
type = "service"
+++

---

Testing, creating visually appealing and responsive user interfaces in Angular.

<!--more-->

I utilized Postman and JMeter to perform comprehensive API testing, ensuring the backend functionalities work smoothly and efficiently. Simultaneously, as a frontend developer with expertise in Angular, I was responsible for creating visually appealing and responsive user interfaces, enhancing the overall user experience and ensuring seamless navigation through the platform. 

It was the job portal for posting and finding job listings, connecting employers with potential candidates and facilitating job seekers in their search for suitable opportunities.