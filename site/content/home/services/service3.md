+++
title = "blu.eberri.es"
dateRange = "2018 - 2020"
type = "service"
+++
***

Building and maintaining web applications using Angular framework and TypeScript programming language.

<!--more-->

https://blu.eberri.es/

---
## Projects:


#### *Piknik dzików*

![piknik dzików](../../../images/piknikdzikow.PNG)

http://piknikdzikow.pl

The sports event promotion website offers a dynamic platform for advertising the upcoming event, complete with an integrated ticket purchasing system, allowing users to conveniently secure their attendance to the event while accessing relevant information and updates.

---


#### *Szybka praca*

![szybkapraca](../../../images/szybkapraca.PNG)

https://szybkapraca-dev.ipv8.pl/

The localized job portal comes with an integrated chat feature, allowing job seekers, freelancers, employers, and clients to communicate effectively and discuss potential opportunities directly within the platform. Moreover, it incorporates a reliable task verification system, ensuring the authenticity and completion of assignments, providing a secure and trustworthy environment for both parties involved in the hiring process.